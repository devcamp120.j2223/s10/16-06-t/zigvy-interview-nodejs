//khai báo express
const express = require("express");

//khai báo mongoose
const mongoose = require("mongoose")

//import các model
const userModel = require("./app/models/userModel");
const postModel = require("./app/models/postModel");
const commentModel = require("./app/models/commentModel");

//khởi tạo app
const app = new express();

//khai báo port
const port = 8000;

//khai báo sử dung json
app.use(express.json());

//khai báo sử dụng unicode
app.use(express.urlencoded({
    extended:true
}));

//kết nối CSDL
mongoose.connect("mongodb://localhost:27017/zigvy-interview", (err) => {
    if(err) {
        throw err;
    }

    console.log("Connect MongoDB successfully!");
})

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})