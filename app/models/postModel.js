//B1: khai báo sử dụng mongoose
const mongoose = require("mongoose");

//B2: khai báo schema
const Schema = mongoose.Schema;

//B3: khơi tạo 1 schema
const postSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    title : {
        type : String,
        unique : true
    },
    body : {
        type : String,
        required : true
    },
    userId : {
        type : mongoose.Types.ObjectId,
        ref : "user"
    }
});

//B4: export model compile từ schema
module.exports = mongoose.model("post", postSchema);